# VaadInfra

Vaadin based presentation layer infrastructure for enterprise Web applications

# Features

- Built on BootInfa and Vaadin 14
- Built-in features inherited from BootInfra project
- Extended UI component Library based on Vaadin 14
- I18N support
