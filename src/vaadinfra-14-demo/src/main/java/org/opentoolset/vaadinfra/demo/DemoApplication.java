package org.opentoolset.vaadinfra.demo;

import org.opentoolset.bootinfra.AbstractApplication;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.VaadInfraApplication;
import org.springframework.boot.SpringApplication;

@VaadInfraApplication
public class DemoApplication extends AbstractApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	public enum I18n implements Translatable {
		TITLE,
	}
}
