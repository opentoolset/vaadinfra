package org.opentoolset.vaadinfra.demo;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.opentoolset.bootinfra.i18n.LocaleWrapper;
import org.opentoolset.vaadinfra.AbstractTranslationProvider;

import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
public class TranslationProvider extends AbstractTranslationProvider {

	private static final long serialVersionUID = 1L;

	public static final Package BASE_PACKAGE = DemoApplication.class.getPackage();
	private static final LocaleWrapper LOCALE_EN = LocaleWrapper.create("en", "US");
	private static final LocaleWrapper LOCALE_TR = LocaleWrapper.create("tr", "TR", "ISO-8859-9");
	public static final List<LocaleWrapper> PROVIDED_LOCALE_WRAPPERS = Arrays.asList(LOCALE_TR, LOCALE_EN);

	@Override
	protected Package getBasePackage() {
		return BASE_PACKAGE;
	}

	@Override
	protected List<LocaleWrapper> getProvidedLocaleWrappers() {
		return PROVIDED_LOCALE_WRAPPERS;
	}

	@Override
	protected Locale getDefaultLocale() {
		return LOCALE_TR.getLocale();
	}
}
