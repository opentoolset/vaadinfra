package org.opentoolset.vaadinfra.demo.backend;

import java.util.Objects;

import org.opentoolset.vaadinfra.VIApi;

public class User implements VIApi.User {

	private String username;

	private String passwordHash;

	private String role;

	public User() {
	}

	public User(String username, String passwordHash, String role) {
		this();
		this.username = username;
		this.passwordHash = passwordHash;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	// ---

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), this.username);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		if (!super.equals(o)) {
			return false;
		}

		User that = (User) o;
		return this.username.equals(that.username);
	}
}
