package org.opentoolset.vaadinfra.demo;

import java.util.Arrays;
import java.util.Set;

import org.opentoolset.bootinfra.AbstractConfig;
import org.opentoolset.bootinfra.BIConfig;

public class DemoConfig extends BIConfig {

	private enum Entry implements AbstractConfig.Entry {

		SAMPLE_ENTRY1("sample.config.entry1", DemoConstants.SAMPLE_CONFIG_ENTRY1),
		SAMPLE_ENTRY2("sample.config.entry2", DemoConstants.SAMPLE_CONFIG_ENTRY2);

		private String key;

		private Object defaultValue;

		private <T> Entry(String key, Object defaultValue) {
			this.key = key;
			this.defaultValue = defaultValue;
		}

		public String getKey() {
			return key;
		}

		public Object getDefaultValue() {
			return defaultValue;
		}
	}

	// ---

	public String getSampleEntry1() {
		return getString(Entry.SAMPLE_ENTRY1);
	}

	public String getSampleEntry2() {
		return getString(Entry.SAMPLE_ENTRY2);
	}

	// ---

	@Override
	protected Set<AbstractConfig.Entry> getEntriesCombined() {
		Set<AbstractConfig.Entry> entries = super.getEntriesCombined();
		entries.addAll(Arrays.asList(Entry.values()));
		return entries;
	}
}
