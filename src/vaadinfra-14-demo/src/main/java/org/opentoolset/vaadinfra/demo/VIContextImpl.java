package org.opentoolset.vaadinfra.demo;

import org.opentoolset.vaadinfra.AbstractTranslationProvider;
import org.opentoolset.vaadinfra.VIApi.VIContext;
import org.opentoolset.vaadinfra.demo.ui.views.misc.LoginView;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
public class VIContextImpl implements VIContext {

	@Autowired
	private AbstractTranslationProvider translationProvider;

	@Override
	public String getAppTitle() {
		return this.translationProvider.getTranslation(DemoApplication.I18n.TITLE);
	}

	@Override
	public Class<? extends Component> getLoginTarget() {
		return LoginView.class;
	}
}
