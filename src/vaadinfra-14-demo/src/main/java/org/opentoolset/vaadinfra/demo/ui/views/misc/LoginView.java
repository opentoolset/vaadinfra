package org.opentoolset.vaadinfra.demo.ui.views.misc;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.components.VILoginView;
import org.opentoolset.vaadinfra.demo.ui.views.masterdetail.MasterDetailView;

import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.router.Route;

@Route(value = VILoginView.ID)
@JsModule("./styles/shared-styles.js")
@Viewport("width=device-width, minimum-scale=1, initial-scale=1, user-scalable=yes, viewport-fit=cover")
public class LoginView extends VILoginView {

	private static final long serialVersionUID = 1L;

	@Override
	public String getViewTitle() {
		return getTranslation(I18n.TITLE);
	}

	protected String getFormTitle() {
		return getTranslation(I18n.TITLE);
	}

	@Override
	protected String getUsernameLabel() {
		return getTranslation(I18n.USERNAME);
	}

	@Override
	protected String getPasswordLabel() {
		return getTranslation(I18n.PASSWORD);
	}

	@Override
	protected String getSubmitButtonTitle() {
		return getTranslation(I18n.TITLE);
	}

	@Override
	protected String getLoginTarget() {
		return MasterDetailView.ID;
	}

	private enum I18n implements Translatable {
		TITLE,
		USERNAME,
		PASSWORD
	}
}
