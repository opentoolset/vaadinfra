package org.opentoolset.vaadinfra.demo.backend;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Employee {

	@JsonIgnore
	private Long id;

	@SuppressWarnings("unused")
	private String idString;
	private String firstname;
	private String lastname;
	private String title;
	private String email;
	private LocalDate startDate;
	private String notes = "";

	public Employee() {
	}

	public Employee(Long id, String firstname, String lastname, String email, String title) {
		this();

		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.title = title;
		this.startDate = LocalDate.now().minusDays(Math.round(Math.random() * 100));
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	// ---

	/**
	 * When transmitting ID to the client we need a data type that is supported (long is not supported in JavaScript)
	 *
	 * @return String representation if {@link #id}
	 */
	public String getIdString() {
		return id == null ? null : id.toString();
	}

	public void setIdString(String idString) {
		// no-op
	}

	@Override
	public String toString() {
		return firstname + " " + lastname + "(" + email + ")";
	}

	@Override
	public int hashCode() {
		if (id == null) {
			return super.hashCode();
		} else {
			return id.intValue();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || id == null) {
			return false;
		}

		if (!(obj instanceof Employee)) {
			return false;
		}

		if (id.equals(((Employee) obj).id)) {
			return true;
		}

		return false;
	}
}
