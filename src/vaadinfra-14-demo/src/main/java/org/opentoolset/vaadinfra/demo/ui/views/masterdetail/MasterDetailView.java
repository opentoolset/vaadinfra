package org.opentoolset.vaadinfra.demo.ui.views.masterdetail;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.components.VIFormLayout;
import org.opentoolset.vaadinfra.components.VIGrid;
import org.opentoolset.vaadinfra.components.VIView;
import org.opentoolset.vaadinfra.demo.backend.BackendService;
import org.opentoolset.vaadinfra.demo.backend.Employee;
import org.opentoolset.vaadinfra.demo.ui.MainView;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;

@Route(value = MasterDetailView.ID, layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@CssImport("styles/views/masterdetail/master-detail-view.css")
public class MasterDetailView extends VIView {

	private static final long serialVersionUID = 1L;

	public static final String ID = "master-detail";

	@Autowired
	private BackendService service;

	private VIGrid<Employee> grid = new VIGrid<>(Employee.class, SelectionMode.SINGLE);

	private TextField firstname = new TextField();
	private TextField lastname = new TextField();
	private TextField email = new TextField();
	private PasswordField password = new PasswordField();

	private Button cancel = new Button("Cancel");
	private Button save = new Button("Save");

	private Binder<Employee> binder;

	public MasterDetailView() {
		super();

		SplitLayout splitLayout = new SplitLayout();
		{
			Div listDiv = buildList(splitLayout);
			Div editorDiv = buildEditor();

			splitLayout.addToPrimary(listDiv);
			splitLayout.addToSecondary(editorDiv);
			splitLayout.setSplitterPosition(100);
			splitLayout.setSizeFull();
		}

		add(splitLayout);
	}

	// ---

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		grid.setItems(service.getEmployees());
	}

	// ---

	@Override
	protected String getViewId() {
		return ID;
	}

	@Override
	protected String getViewTitle() {
		return getTranslation(I18n.TITLE);
	}

	// ---

	private Div buildList(SplitLayout splitLayout) {
		Div listDiv = new Div();
		{
			{
				grid.setColumns();
				grid.addColumn(Employee::getFirstname).setHeader("First name");
				grid.addColumn(Employee::getLastname).setHeader("Last name");
				grid.addColumn(Employee::getEmail).setHeader("Email");
				grid.addColumn(Employee::getStartDate).setHeader("Start date");
				grid.setSortableColumns();
				grid.addSingleValueChangeListener(event -> onEmployeeSelectionChanged(event, splitLayout));
			}

			listDiv.add(grid);
			listDiv.setId("wrapper");
			listDiv.setWidthFull();
			listDiv.setMinWidth("300px");
		}

		return listDiv;
	}

	private Div buildEditor() {
		Div editorDiv = new Div();
		{
			VIFormLayout formLayout = new VIFormLayout();
			{
				binder = new Binder<>(Employee.class);
				{
					binder.bindInstanceFields(this);
				}
				formLayout.add(firstname, "First name");
				formLayout.add(lastname, "Last name");
				formLayout.add(email, "Email");
				formLayout.add(password, "Password");
			}

			HorizontalLayout buttonLayout = new HorizontalLayout();
			{
				// the grid valueChangeEvent will clear the form too
				cancel.addClickListener(e -> grid.clearSelection());
				cancel.addThemeVariants(ButtonVariant.LUMO_TERTIARY);

				save.addClickListener(e -> Notification.show("Not implemented"));
				save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

				buttonLayout.setId("button-layout");
				buttonLayout.add(cancel, save);
				buttonLayout.setWidthFull();
				buttonLayout.setSpacing(true);
			}

			editorDiv.setId("editor-layout");
			editorDiv.add(formLayout, buttonLayout);
			editorDiv.setSizeFull();
			editorDiv.setMinWidth("400px");
			editorDiv.setVisible(false);
		}

		return editorDiv;
	}

	private void onEmployeeSelectionChanged(ComponentValueChangeEvent<?, Employee> event, SplitLayout splitLayout) {
		// Value can be null as well, that clears the form
		Employee employee = event.getValue();
		binder.readBean(employee);

		// The password field isn't bound through the binder, so handle that
		password.setValue("");

		Component secondaryComponent = splitLayout.getSecondaryComponent();
		if (employee != null) {
			binder.readBean(employee);
			if (!secondaryComponent.isVisible()) {
				secondaryComponent.setVisible(true);
			}
		} else {
			secondaryComponent.setVisible(false);
		}
	}

	private enum I18n implements Translatable {
		TITLE
	}
}
