package org.opentoolset.vaadinfra.demo.ui.views.empty;

import org.opentoolset.vaadinfra.demo.ui.MainView;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route(value = "empty", layout = MainView.class)
@PageTitle("Empty")
@CssImport("styles/views/empty/empty-view.css")
public class EmptyView extends Div {

	private static final long serialVersionUID = 1L;

	public EmptyView() {
		setId("empty-view");
		add(new Label("Content placeholder"));
	}
}
