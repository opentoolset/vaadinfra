package org.opentoolset.vaadinfra.demo.ui;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.components.VIAnchor;
import org.opentoolset.vaadinfra.components.VIAppLayout;
import org.opentoolset.vaadinfra.components.VIRouterLink;
import org.opentoolset.vaadinfra.components.VITab;
import org.opentoolset.vaadinfra.components.VITabGroup;
import org.opentoolset.vaadinfra.demo.ui.views.cardlist.CardListView;
import org.opentoolset.vaadinfra.demo.ui.views.empty.EmptyView;
import org.opentoolset.vaadinfra.demo.ui.views.form.FormView;
import org.opentoolset.vaadinfra.demo.ui.views.masterdetail.MasterDetailView;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.VaadinServlet;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

/**
 * The main view is a top-level placeholder for other views.
 */
@JsModule("./styles/shared-styles.js")
@PWA(name = "VaadInfra Demo", shortName = "VaadInfra Demo")
@Theme(value = Lumo.class, variant = Lumo.LIGHT)
public class MainView extends VIAppLayout {

	private static final long serialVersionUID = 1L;

	private VITabGroup menu;

	// ---

	@Override
	protected void afterNavigation() {
		super.afterNavigation();
		selectTab();
	}

	// ---

	@PostConstruct
	private void postConstruct() {
		setPrimarySection(Section.DRAWER);
		addToNavbar(true, new DrawerToggle());

		menu = buildMenu();
		addToDrawer(menu);
		setDrawerOpened(false);
	}

	private VITabGroup buildMenu() {
		VITabGroup tabs = new VITabGroup();
		tabs.add(createTab(I18n.MASTER_DETAIL, VaadinIcon.TWIN_COL_SELECT, MasterDetailView.class));
		tabs.add(createTab(I18n.CARD_LIST, VaadinIcon.USER_CARD, CardListView.class));
		tabs.add(createTab(I18n.FORM, VaadinIcon.FORM, FormView.class));
		tabs.add(createTab(I18n.COMPONENTS, VaadinIcon.VIEWPORT, EmptyView.class));
		tabs.add(createTab(createLogoutLink(I18n.LOGOUT)));
		return tabs;
	}

	private VITab createTab(Translatable translatable, VaadinIcon icon, Class<? extends Component> viewClass) {
		return createTab(populateLink(new VIRouterLink(viewClass), icon, getTranslation(translatable)));
	}

	private VITab createTab(Component content) {
		VITab tab = new VITab();
		tab.add(content);
		return tab;
	}

	private VIAnchor createLogoutLink(Translatable translatable) {
		String contextPath = VaadinServlet.getCurrent().getServletContext().getContextPath();
		VIAnchor anchor = populateLink(new VIAnchor(contextPath + "/logout"), VaadinIcon.SIGN_OUT, getTranslation(translatable));
		return anchor;
	}

	private <T extends HasComponents> T populateLink(T hasComponents, VaadinIcon icon, String title) {
		hasComponents.add(icon.create());
		populateLink(hasComponents, title);
		return hasComponents;
	}

	private <T extends HasComponents> T populateLink(T hasComponents, String title) {
		hasComponents.add(title);
		return hasComponents;
	}

	private void selectTab() {
		Class<? extends Component> navigationTargetClass = getContent().getClass();
		String target = RouteConfiguration.forSessionScope().getUrl(navigationTargetClass);
		Optional<VITab> tabToSelect = menu.getVITabs().filter(tab -> containsLinkToTarget(tab, target)).findFirst();
		tabToSelect.ifPresent(tab -> menu.setSelectedTab(tab));
	}

	private boolean containsLinkToTarget(VITab tab, String target) {
		return tab.getDescendants().stream().anyMatch(child -> containsLinkToTarget(child, target));
	}

	private boolean containsLinkToTarget(Component component, String target) {
		return component instanceof VIRouterLink && ((VIRouterLink) component).getHref().equals(target);
	}

	private enum I18n implements Translatable {
		MASTER_DETAIL,
		FORM,
		CARD_LIST,
		COMPONENTS,
		LOGOUT
	}
}
