// ---
// Copyright 2020 BootInfra Team
// All rights reserved
// ---
package org.opentoolset.vaadinfra.demo;

import java.nio.file.Paths;

import org.opentoolset.bootinfra.SpringConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoSpringConfiguration extends SpringConfiguration {

	@Override
	protected String getAppHomeFolder() {
		return Paths.get(System.getProperty("user.home")).resolve("vaadinfra-demo").toString();
	}

	@Override
	protected DemoConfig buildConfig() {
		return new DemoConfig();
	}
}
