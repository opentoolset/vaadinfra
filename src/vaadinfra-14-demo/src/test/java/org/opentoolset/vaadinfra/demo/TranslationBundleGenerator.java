package org.opentoolset.vaadinfra.demo;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.opentoolset.bootinfra.BIContext;
import org.opentoolset.bootinfra.BIContext.Mode;
import org.opentoolset.bootinfra.i18n.I18nBundleGenerator;
import org.opentoolset.bootinfra.i18n.LocaleWrapper;

public class TranslationBundleGenerator {

	static {
		BIContext.mode = Mode.POPULATOR;
	}

	@Test
	public void generate() {
		Package basePackage = TranslationProvider.BASE_PACKAGE;
		List<LocaleWrapper> providedLocaleWrappers = TranslationProvider.PROVIDED_LOCALE_WRAPPERS;
		I18nBundleGenerator bundleGenerator = new I18nBundleGenerator.Builder(basePackage).setProvidedLocaleWrappers(providedLocaleWrappers).build();
		bundleGenerator.generate();
	}
}
