package org.opentoolset.vaadinfra.components;

import java.util.List;
import java.util.Set;

import org.opentoolset.vaadinfra.VIConversionUtils;

import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.shared.Registration;

public class VIGrid<T> extends Composite<Div> {

	private static final long serialVersionUID = 1L;

	private Grid<T> grid;

	private SelectionMode selectionMode;

	public VIGrid(SelectionMode mode) {
		grid = new Grid<>();
		initialize(mode);
	}

	public VIGrid(Class<T> beanType, SelectionMode mode) {
		grid = new Grid<>(beanType);
		initialize(mode);
	}

	public Column<T> addColumn(ValueProvider<T, ?> valueProvider) {
		return grid.addColumn((item) -> {
			Object modelValue = valueProvider.apply(item);
			String presentationValue = VIConversionUtils.convert(modelValue);
			return presentationValue;
		});
	}

	public void setColumns(String... propertyNames) {
		grid.setColumns(propertyNames);
	}

	public void setSortableColumns(String... propertyNames) {
		grid.setSortableColumns(propertyNames);
	}

	public void setItems(List<T> items) {
		grid.setItems(items);
	}

	public Registration addSingleValueChangeListener(ValueChangeListener<ComponentValueChangeEvent<?, T>> listener) {
		return grid.asSingleSelect().addValueChangeListener(listener);
	}

	public Registration addMultiValueChangeListener(ValueChangeListener<ComponentValueChangeEvent<?, Set<T>>> listener) {
		return grid.asMultiSelect().addValueChangeListener(listener);
	}

	public void clearSelection() {
		switch (selectionMode) {
			case SINGLE:
				grid.asSingleSelect().clear();
				break;

			case MULTI:
				grid.asMultiSelect().clear();
				break;

			default:
				break;
		}
	}

	// ---

	private void initialize(SelectionMode mode) {
		this.selectionMode = mode;
		grid.setSelectionMode(mode);
		grid.addThemeVariants(GridVariant.LUMO_NO_BORDER);
		grid.setSizeFull();

		getContent().add(grid);
		getContent().setSizeFull();
	}
}
