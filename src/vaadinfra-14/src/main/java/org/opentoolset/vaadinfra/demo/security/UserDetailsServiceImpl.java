package org.opentoolset.vaadinfra.demo.security;

import java.util.Collections;

import org.opentoolset.vaadinfra.VIApi;
import org.opentoolset.vaadinfra.VIApi.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

	private UserService userService;

	@Autowired
	public UserDetailsServiceImpl(UserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		VIApi.User user = this.userService.getUser(username);
		if (user != null) {
			return new User(user.getUsername(), user.getPasswordHash(), Collections.singletonList(new SimpleGrantedAuthority(user.getRole())));
		} else {
			throw new UsernameNotFoundException("No user present with username: " + username);
		}
	}
}