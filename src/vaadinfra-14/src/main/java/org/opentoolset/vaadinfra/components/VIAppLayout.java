package org.opentoolset.vaadinfra.components;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.AbstractTranslationProvider;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.applayout.AppLayout;

public abstract class VIAppLayout extends AppLayout {

	private static final long serialVersionUID = 1L;

	@Autowired
	private AbstractTranslationProvider translationProvider;

	protected String getTranslation(Translatable translatable) {
		return this.translationProvider.getTranslation(translatable);
	}
}
