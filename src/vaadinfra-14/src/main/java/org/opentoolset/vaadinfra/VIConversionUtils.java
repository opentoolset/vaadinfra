package org.opentoolset.vaadinfra;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.vaadin.flow.function.SerializableFunction;

public class VIConversionUtils {

	public static String dateFormat = "yyyy-MM-dd";

	public static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";

	private static final SimpleDateFormat FORMATTER_SIMPLE_DATE = new SimpleDateFormat(dateTimeFormat);
	private static final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern(dateFormat);
	private static final DateTimeFormatter FORMATTER_DATE_TIME = DateTimeFormatter.ofPattern(dateTimeFormat);

	public interface Converters {

		SerializableFunction<Date, String> FROM_DATE = time -> getFormatterSimpleDate().format(time);

		SerializableFunction<LocalDate, String> FROM_LOCAL_DATE = time -> getFormatterLocalDate().format(time);

		SerializableFunction<LocalDateTime, String> FROM_LOCAL_DATE_TIME = time -> getFormatterLocalDateTime().format(time);
	}

	public static SimpleDateFormat getFormatterSimpleDate() {
		return FORMATTER_SIMPLE_DATE;
	}

	public static DateTimeFormatter getFormatterLocalDate() {
		return FORMATTER_DATE;
	}

	public static DateTimeFormatter getFormatterLocalDateTime() {
		return FORMATTER_DATE_TIME;
	}

	public static String convert(Object modelValue) {
		if (modelValue instanceof LocalDate) {
			LocalDate time = (LocalDate) modelValue;
			return Converters.FROM_LOCAL_DATE.apply(time);
		}

		if (modelValue instanceof LocalDateTime) {
			LocalDateTime time = (LocalDateTime) modelValue;
			return Converters.FROM_LOCAL_DATE_TIME.apply(time);
		}

		if (modelValue instanceof Date) {
			Date time = (Date) modelValue;
			return Converters.FROM_DATE.apply(time);
		}

		return modelValue != null ? modelValue.toString() : null;
	}
}
