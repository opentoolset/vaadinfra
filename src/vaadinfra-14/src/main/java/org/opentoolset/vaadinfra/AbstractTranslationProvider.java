package org.opentoolset.vaadinfra;

import java.util.List;
import java.util.Locale;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider;
import org.slf4j.LoggerFactory;

import com.vaadin.flow.i18n.I18NProvider;
import com.vaadin.flow.server.ServiceException;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.SessionInitEvent;
import com.vaadin.flow.server.SessionInitListener;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.VaadinSession;

public abstract class AbstractTranslationProvider extends AbstractI18nProvider implements I18NProvider, VaadinServiceInitListener, SessionInitListener {

	private static final long serialVersionUID = 1L;

	// ---

	@Override
	public List<Locale> getProvidedLocales() {
		return super.getLocales();
	}

	@Override
	public void serviceInit(ServiceInitEvent event) {
		event.getSource().addSessionInitListener(this);
	}

	@Override
	public void sessionInit(SessionInitEvent event) throws ServiceException {
		event.getSession().setLocale(getDefaultLocale());
	}

	@Override
	public Locale getLocale() {
		VaadinSession session = VaadinSession.getCurrent();
		if (session == null) {
			LoggerFactory.getLogger(AbstractTranslationProvider.class.getName()).warn("No session binded to current thread");
			return null;
		}

		Locale locale = session.getLocale();
		if (locale == null) {
			locale = getDefaultLocale();
		}

		return locale;
	}
}
