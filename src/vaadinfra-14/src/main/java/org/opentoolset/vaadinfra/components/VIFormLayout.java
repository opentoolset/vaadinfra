package org.opentoolset.vaadinfra.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;

public class VIFormLayout extends Composite<Div> {

	private static final long serialVersionUID = 1L;

	private FormLayout formLayout = new FormLayout();

	public VIFormLayout() {
		getContent().add(this.formLayout);
	}

	public void add(Component... components) {
		formLayout.add(components);
	}

	public void add(Component component, String label) {
		component.getElement().setProperty("label", label == null ? "" : label);
		formLayout.add(component);
	}

	public void setReadonly(boolean readOnly) {
		setReadonly(formLayout, readOnly);
	}

	private void setReadonly(Component component, boolean readOnly) {
		if (component instanceof HasValue) {
			HasValue<?, ?> hasValue = (HasValue<?, ?>) component;
			hasValue.setReadOnly(true);
		}

		component.getChildren().forEach(child -> setReadonly(child, readOnly));
	}
}
