package org.opentoolset.vaadinfra.components;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tab;

public class VITab extends Composite<Div> {

	private static final long serialVersionUID = 1L;

	private Tab tab;

	public VITab() {
		this.tab = new Tab();
		initialize();
	}

	public Tab getTab() {
		return tab;
	}

	public void add(Component... components) {
		tab.add(components);
	}

	public List<Component> getDescendants() {
		List<Component> descendants = getDescendants(tab);
		return descendants;
	}

	// ---

	private List<Component> getDescendants(Component component) {
		List<Component> descendants = new ArrayList<>();
		descendants.add(component);
		List<Component> children = component.getChildren().collect(Collectors.toList());
		for (Component child : children) {
			descendants.addAll(getDescendants(child));
		}
		return descendants;
	}

	private void initialize() {
		getContent().add(tab);
		getContent().setSizeFull();
	}
}
