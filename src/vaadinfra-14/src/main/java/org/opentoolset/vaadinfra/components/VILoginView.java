package org.opentoolset.vaadinfra.components;

import javax.annotation.PostConstruct;

import org.opentoolset.vaadinfra.VIApi.VIContext;
import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.AbstractTranslationProvider;
import org.opentoolset.vaadinfra.demo.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.HasDynamicTitle;

public abstract class VILoginView extends LoginOverlay implements AfterNavigationObserver, BeforeEnterObserver, HasDynamicTitle {

	private static final long serialVersionUID = 1L;

	public static final String ID = "login";

	@Autowired
	private VIContext config;

	@Autowired
	private AbstractTranslationProvider translationProvider;

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		if (SecurityUtils.isUserLoggedIn()) {
			Class<? extends Component> loginTargetType = getLoginTargetType();
			if (loginTargetType != null) {
				event.forwardTo(loginTargetType);
				return;
			}

			String loginTarget = getLoginTarget();
			event.forwardTo(loginTarget);
		} else {
			setOpened(true);
		}
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		setError(event.getLocation().getQueryParameters().getParameters().containsKey("error"));
	}

	@Override
	public String getPageTitle() {
		return String.format("%s - %s", config.getAppTitle(), getViewTitle());
	}

	// ---

	protected String getViewId() {
		return ID;
	}

	protected String getLoginTarget() {
		return "";
	}

	protected Class<? extends Component> getLoginTargetType() {
		return null;
	}

	protected String getViewTitle() {
		return "Login";
	}

	protected String getHeaderTitle() {
		return config.getAppTitle();
	}

	protected String getLoginFormDescription() {
		return "";
	}

	protected String getFormTitle() {
		return "Sign in";
	}

	protected String getUsernameLabel() {
		return "Username";
	}

	protected String getPasswordLabel() {
		return "Password";
	}

	protected String getSubmitButtonTitle() {
		return "Sign in";
	}

	protected String getLoginFormAdditionalInformation() {
		return "";
	}

	protected String getTranslation(Translatable translatable) {
		return this.translationProvider.getTranslation(translatable);
	}

	@PostConstruct
	protected void postConstruct() {
		LoginI18n login = LoginI18n.createDefault();
		{
			LoginI18n.Header header = new LoginI18n.Header();
			{
				header.setTitle(getHeaderTitle());
				header.setDescription(getLoginFormDescription());
				login.setAdditionalInformation(getLoginFormAdditionalInformation());
				login.setHeader(header);
			}

			LoginI18n.Form form = new LoginI18n.Form();
			{
				form.setSubmit(getSubmitButtonTitle());
				form.setTitle(getFormTitle());
				form.setUsername(getUsernameLabel());
				form.setPassword(getPasswordLabel());
				login.setForm(form);
			}

			setI18n(login);
		}

		// setForgotPasswordButtonVisible(true);
		setAction(getViewId());
		// getTranslation(key, params);
	}
}
