package org.opentoolset.vaadinfra.components;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.Tabs.Orientation;

public class VITabGroup extends Composite<Div> {

	private static final long serialVersionUID = 1L;

	private Tabs tabs = new Tabs();

	private List<VITab> viTabs = new ArrayList<>();

	public VITabGroup() {
		initialize();
	}

	public void setOrientation(Orientation orientation) {
		tabs.setOrientation(orientation);
	}

	public void add(VITab tab) {
		add(tab.getTab());
		viTabs.add(tab);
	}

	public void add(Tab tab) {
		tabs.add(tab);
	}

	public void setSelectedTab(VITab selectedTab) {
		Tab tab = selectedTab.getTab();
		tabs.setSelectedTab(tab);
	}

	public void setSelectedTab(Tab selectedTab) {
		tabs.setSelectedTab(selectedTab);
	}

	public Stream<VITab> getVITabs() {
		return viTabs.stream().filter(tab -> tab instanceof VITab).map(component -> (VITab) component);
	}

	public Stream<Tab> getTabs() {
		return tabs.getChildren().filter(component -> component instanceof Tab).map(component -> (Tab) component);
	}

	// ---

	private void initialize() {
		tabs.setId("tabs");
		tabs.setOrientation(Tabs.Orientation.VERTICAL);

		getContent().add(tabs);
		getContent().setSizeFull();
	}
}
