package org.opentoolset.vaadinfra;

import com.vaadin.flow.component.Component;

public interface VIApi {

	/**
	 * Needed as spring component
	 */
	public interface VIContext {

		String getAppTitle();

		Class<? extends Component> getLoginTarget();
	}

	/**
	 * Needed as spring service
	 */
	public interface UserService {

		User getUser(String username);
	}

	public interface User {

		String getUsername();

		String getPasswordHash();

		String getRole();
	}
}
