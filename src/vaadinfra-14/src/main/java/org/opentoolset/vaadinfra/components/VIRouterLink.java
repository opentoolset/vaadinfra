package org.opentoolset.vaadinfra.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.RouterLink;

public class VIRouterLink extends Composite<Div> implements HasComponents {

	private static final long serialVersionUID = 1L;

	private RouterLink routerLink;
	private HorizontalLayout layout;

	public VIRouterLink(Class<? extends Component> viewClass) {
		this.layout = new HorizontalLayout();
		this.layout.setDefaultVerticalComponentAlignment(Alignment.START);
		this.layout.setSizeUndefined();

		this.routerLink = new RouterLink(null, viewClass);
		this.routerLink.add(this.layout);
		getContent().add(this.routerLink);
	}

	@Override
	public void add(Component... components) {
		this.layout.add(components);
	}

	@Override
	public void add(String text) {
		Span span = new Span(text);
		span.setTitle(text);
		this.layout.add(span);
	}

	public void addHTML(String html) {
		Span span = new Span();
		span.getElement().setProperty("innerHTML", html);
		this.layout.add(span);
	}

	public String getHref() {
		return this.routerLink.getHref();
	}

	@Override
	public void remove(Component... components) {
		this.layout.remove(components);
	}

	@Override
	public void removeAll() {
		this.layout.removeAll();
	}

	@Override
	public void addComponentAsFirst(Component component) {
		this.layout.addComponentAsFirst(component);
	}

	@Override
	public void addComponentAtIndex(int index, Component component) {
		this.layout.addComponentAtIndex(index, component);
	}
}
