package org.opentoolset.vaadinfra.components;

import org.opentoolset.bootinfra.i18n.AbstractI18nProvider.Translatable;
import org.opentoolset.vaadinfra.AbstractTranslationProvider;
import org.opentoolset.vaadinfra.VIApi.VIContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.HasDynamicTitle;

public abstract class VIView extends Div implements AfterNavigationObserver, HasDynamicTitle {

	private static final long serialVersionUID = 1L;

	@Autowired
	private VIContext config;

	@Autowired
	private AbstractTranslationProvider translationProvider;

	protected abstract String getViewId();

	public VIView() {
		String id = getViewId();
		if (id != null) {
			id = String.format("%s-view", id);
			setId(id);
		}

		setSizeFull();
	}

	@Override
	public String getPageTitle() {
		return String.format("%s - %s", config.getAppTitle(), getViewTitle());
	}

	// ---

	protected String getViewTitle() {
		return "";
	}

	protected String getTranslation(Translatable translatable) {
		return this.translationProvider.getTranslation(translatable);
	}
}
