package org.opentoolset.vaadinfra.demo.views.empty;

import com.vaadin.flow.server.connect.Endpoint;
import com.vaadin.flow.server.connect.auth.AnonymousAllowed;

/**
 * The endpoint for the client-side view.
 */
@Endpoint
@AnonymousAllowed
public class EmptyEndpoint {
}
